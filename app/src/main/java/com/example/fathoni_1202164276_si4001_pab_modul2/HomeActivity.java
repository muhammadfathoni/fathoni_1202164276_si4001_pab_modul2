package com.example.fathoni_1202164276_si4001_pab_modul2;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Timer;


public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView isiSld, tgl1,wib, wit,inTg,tgl2, wib2,inWw,txtiket;
    Button topup, nywn;
    String kota, harga;
    private int tahun, hari, bulan, jam, menit, kota_tujuan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        topup =findViewById(R.id.buttontopup);
        topup.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final Dialog topup_dialog = new Dialog(HomeActivity.this);

                topup_dialog.setContentView(R.layout.topup);
                topup_dialog.setTitle("ISI DULU SALDONYA!");

                final EditText isi_saldo = topup_dialog.findViewById(R.id.isi_saldo);
                Button cancel = topup_dialog.findViewById(R.id.btnCancel);
                final Button btnTop = topup_dialog.findViewById(R.id.btnYes);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        topup_dialog.dismiss();
                    }
                });

                btnTop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        isiSld = findViewById(R.id.isiSld);

                        int tmbhan = Integer.parseInt(isi_saldo.getText().toString());
                        int saldo_crnt = Integer.parseInt(isiSld.getText().toString());
                        int tot = saldo_crnt + tmbhan;

                        isiSld.setText(String.valueOf(tot));

                        topup_dialog.dismiss();
                    }
                });

                topup_dialog.show();
            }
        });



        tgl1 = findViewById(R.id.inTanggal);
        tgl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                tahun = c.get(Calendar.YEAR);
                bulan = c.get(Calendar.MONTH);
                hari = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog DatePicker = new DatePickerDialog(HomeActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
                        tgl1.setText(dayOfMonth + "/" + month + "/" + year);
                    }
                },tahun,bulan,hari);
                DatePicker.show();
            }
        });

        wib = findViewById(R.id.inWaktu);
        wib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                jam   = c.get(Calendar.HOUR_OF_DAY);
                menit = c.get(Calendar.MINUTE);

                TimePickerDialog TimeAmbil = new TimePickerDialog(HomeActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        wib.setText(hourOfDay + ":" + minute);
                    }
                },jam, menit, true);
                TimeAmbil.show();
            }
        });
        final Switch swtch = findViewById(R.id.switch1);
        swtch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(swtch.isChecked()){
                    inWw = findViewById(R.id.inWaktu2);
                    inTg = findViewById(R.id.inTanggal2);

                    inWw.setVisibility(View.VISIBLE);
                    inTg.setVisibility(View.VISIBLE);
                } else {
                    inWw.setVisibility(View.INVISIBLE);
                    inTg.setVisibility(View.INVISIBLE);
                }
            }
        });

        tgl2 = findViewById(R.id.inTanggal2);
        tgl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                tahun = c.get(Calendar.YEAR);
                bulan = c.get(Calendar.MONTH);
                hari = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog DatePicker = new DatePickerDialog(HomeActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {
                        tgl1.setText(dayOfMonth + "/" + month + "/" + year);
                    }
                },tahun,bulan,hari);
                DatePicker.show();
            }
        });

        wib2 = findViewById(R.id.inWaktu2);
        wib2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                jam   = c.get(Calendar.HOUR_OF_DAY);
                menit = c.get(Calendar.MINUTE);

                TimePickerDialog TimeAmbil = new TimePickerDialog(HomeActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        wib.setText(hourOfDay + ":" + minute);
                    }
                },jam, menit, true);
                TimeAmbil.show();
            }
        });

        nywn = findViewById(R.id.btnBeli);
        nywn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                txtiket = findViewById(R.id.inTikett);
                int tiket = Integer.parseInt(txtiket.getText().toString());
                int biaya_jalan = Integer.parseInt(harga);
                int total_biaya = tiket * biaya_jalan;

                int saldo_curr = Integer.parseInt(isiSld.getText().toString());

                if (saldo_curr < total_biaya) {
                    Toast.makeText(HomeActivity.this,"Saldo Anda Tidak Cukup \n Silahkan Top Up Terlebih Dahulu",
                            Toast.LENGTH_SHORT).show();
                } else if (total_biaya == 0) {
                    Toast.makeText(HomeActivity.this,"Isi Form Dengan Lengkap",Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(HomeActivity.this, CheckoutActivity.class);
                    Bundle info = new Bundle();
                    String tgl_brkt = tgl1.getText().toString();
                    String jam_brkt = wib.getText().toString();



                    info.putString("tujuan",kota);
                    info.putString("tgl_brkt",tgl_brkt);
                    info.putString("jam_brkt",jam_brkt);
                    info.putInt("saldo",saldo_curr);

                    if(swtch.isChecked()) {
                        info.putInt("swtch",1);
                        info.putString("tgl_blk",inTg.getText().toString());
                        info.putString("jam_blk",inWw.getText().toString());
                        total_biaya += total_biaya;
                    }

                    info.putString("total", String.valueOf(total_biaya));
                    intent.putExtras(info);
                    HomeActivity.this.startActivity(intent);
                }


            }
        });

        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.lokasi, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

                }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        kota_tujuan = parent.getSelectedItemPosition();

        String[] biaya_array = getResources().getStringArray(R.array.biaya_array);
        harga = String.valueOf(biaya_array[kota_tujuan]);

        String[] kota_array = getResources().getStringArray(R.array.kota_array);
        kota = String.valueOf(kota_array[kota_tujuan]);

        Toast.makeText(getBaseContext(), kota, Toast.LENGTH_LONG).show();

        Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}





